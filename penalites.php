<?php include('header.php'); ?>
<?php 
  include_once('controller/connect.php');
  
  $dbs = new database();
  $db=$dbs->connection();
  $type=""; 
  $name=""; 
  $montant="";
  $idPenalite="";
  //$sql = mysqli_query($db,"select * from city  ORDER BY Name");
  $staten = mysqli_query($db,"select * from penalites  ORDER BY Name");
  if(isset($_GET['penEdit']))
  {
    $cityid = $_GET['penEdit'];
    $edit = mysqli_query($db,"select * from penalites where idPenalite='$cityid'");
    $row = mysqli_fetch_assoc($edit);
    $name = $row['nomPenalite'];
    $type = $row['type'];
    $montant = $row['montant'];
    $idPenalite=$row['idPenalite'];
  }

  $page="";
  if(isset($_GET['serachPenalite']))
  {
    $searchNamePenalite = $_GET['serachPenalite'];
    $RecordeLimit = 5;
    $searchPen = mysqli_query($db,"select count(idPenalite) as total from penalites where nomPenalite like '%".$searchNamePenalite."%'");
    $CName = mysqli_fetch_array($searchPen);
    
    $number_of_row =ceil($CName['total']/5); 
    if(isset($_GET['bn']) &&intval($_GET['bn']) <= $number_of_row && intval($_GET['bn'] !=0))
    {
      $Skip = (intval($_GET["bn"]) * $RecordeLimit) - $RecordeLimit;
      $sql = mysqli_query($db,"select * from penalites where nomPenalite like '%".$searchNamePenalite."%' LIMIT $Skip,$RecordeLimit ");
    }
    else
    {
      $sql = mysqli_query($db,"select * from penalites where nomPenalite like '%".$searchNamePenalite."%' LIMIT $RecordeLimit ");
    }

    for($i=0;$i<$number_of_row;$i++)
    {
      $d = $i+1;
      if(isset($_GET["serachPenalite"]))
      {
        $page .= "<a href='penalites.php?serachPenalite=$searchNamePenalite&bn=$d'>$d</a>&nbsp &nbsp &nbsp";
      }
      else
      {
        $page .= "<a href='penalites.php?bn=$d'>$d</a>&nbsp &nbsp &nbsp";
      }                     
    } 
  }
  else
  {
    $RecordeLimit = 5;
    $searchPen= mysqli_query($db,"select count(idPenalite) as total from penalites ");
    $CName = mysqli_fetch_array($searchPen);
    
    $number_of_row =ceil($CName['total']/5);
    if(isset($_GET['bn']) && intval($_GET['bn']) <= $number_of_row && intval($_GET['bn'] != 0 ))
    {
      $Skip = (intval($_GET["bn"]) * $RecordeLimit) - $RecordeLimit;
      $sql = mysqli_query($db,"select * from penalites LIMIT $Skip,$RecordeLimit");
    }
    else
    {
      $sql = mysqli_query($db,"select * from penalites LIMIT $RecordeLimit");
    }

    for($i=0;$i<$number_of_row;$i++)
    {
        $d = $i+1;
        $page .= "<a href='penalites.php?bn=$d'>$d</a>&nbsp &nbsp &nbsp";
    }
  }
?>
<link rel="stylesheet" type="text/css" href="css/table-style.css" />
<link rel="stylesheet" type="text/css" href="css/basictable.css" />
<script type="text/javascript" src="js/jquery.basictable.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#table').basictable();

      $('#table-breakpoint').basictable({
        breakpoint: 768
      });

      $('#table-swap-axis').basictable({
        swapAxis: true
      });

      $('#table-force-off').basictable({
        forceResponsive: false
      });

      $('#table-no-resize').basictable({
        noResize: true
      });

      $('#table-two-axis').basictable();

      $('#table-max-height').basictable({
        tableWrapper: true
      });
    });
</script>
<ol class="breadcrumb" style="margin: 10px 0px ! important;">
    <li class="breadcrumb-item"><a href="Home.php">Accueil</a><i class="fa fa-angle-right"></i>Parametrage<i class="fa fa-angle-right"></i>Penalités</li>
</ol>

<div class="validation-system" style="margin-top: 0;">
    
    <div class="validation-form" style="overflow: auto; margin-right:20px; height: 450px; width: 49%; float: left;">
  <!---->
        <form method="POST" action="controller/ccity.php?penEdit=<?php $idPenalite ?>">
        <div class="vali-form-group" >
        <h2>Ajouter</h2>
          <div class="col-md-3 control-label">
              <label class="control-label">Penalités</label>
              <div class="input-group">             
                  <span class="input-group-addon">
              <i class="fa fa-map-marker" aria-hidden="true"></i>
              </span>
              <select name="type" style="text-transform: capitalize; width: 250px;" required>
                <option value="">-- Penalités pour les --</option>
                  <option value="1" <?php if($type==1){ echo "Selected"; }?>> Vignettes </option>
                  <option value="2" <?php if($type==2){ echo "Selected"; }?>> Patentes </option>

              </select>
              </div>
            </div>
            <div class="clearfix"> </div>
       

            <div class="col-md-3 control-label">
              <label class="control-label">Nom</label>
                <div class="input-group">             
                    <span class="input-group-addon">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                  </span>
                <input type="text" name="nom" placeholder="Nom Penalités" value="<?php echo $name; ?>" required="" class="form-control" style="width: 250px; height: 35px;">
                </div>
            </div>
              <div class="clearfix"> </div>
              <div class="col-md-3 control-label">
              <label class="control-label">Montant</label>
                <div class="input-group">             
                    <span class="input-group-addon">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                  </span>
                <input type="text" name="montant" placeholder="Montant Penalités" value="<?php echo $montant; ?>" required="" class="form-control" style="width: 250px; height: 35px;">
                </div>
            </div>
            <div class="clearfix"> </div>

              <input type="hidden" id="custId" name="idPenalite" value="<?php echo $row['idPenalite']; ?>">

        </div>
            <div class="col-md-12 form-group">
              <button type="submit" name="penalites" class="btn btn-primary">Ajouter</button>
              <button type="reset" class="btn btn-default">Annuler</button>
              
            </div>
          <div class="clearfix"> </div>
        </form>
  <!---->
 </div>
 <div class="validation-form" style="width: 49%; overflow: auto;">
    <div style="height: 396px;">
          <div class="w3l-table-info" >
            <h2>Penalités</h2>
            <br>

            <form method="GET" action="#">
              <input style="float: right;" type="submit" name="searchcity" >
              <input style="float: right;" placeholder="Rechercher" type="search-box" name="serachPenalite" value="<?php echo(isset($_GET['serachPenalite']))?$_GET['serachPenalite']:"";?>"><br>
            </form> 
              <table id="table">
            <thead>
              <tr>
                <th>Id</th>
              <th style="width: 5000px;">Nom</th>
              <th style="width: 500px;">Type</th>
              <th style="width: 300px;">Montant</th>

              <th  style="text-align: center; width: 550px;">Action</th>
              </tr>
            </thead>
            <tbody>
            <?php $i=1; while($row = mysqli_fetch_assoc($sql)) { ?> 
            <tr>
              <td><?php echo ($row['idPenalite']); ?></td>
              <td><?php echo ucfirst($row['nomPenalite']); ?></td>
              <td><?php echo ($row['type']== 1? "Vignettes": "Patentes"); ?></td>
              <td><?php echo ucfirst($row['montant']); ?></td>

              <td><a href="?penEdit=<?php echo $row['idPenalite']; ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
             </tr>  
              <?php } ?>
            </tbody>
            </table>
            <div><?php echo $page; ?></div>
          </div>
    </div>
 </div>
</div>
<?php include('footer.php'); ?>