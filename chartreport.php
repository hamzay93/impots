<?php
 
$dataPoints = array();
//Best practice is to create a separate file for handling connection to database
try{
     // Creating a new connection.
    // Replace your-hostname, your-db, your-username, your-password according to your database
    $link = new \PDO(   'mysql:host=localhost;dbname=dbimpots;charset=utf8mb4', //'mysql:host=localhost;dbname=canvasjs_db;charset=utf8mb4',
                        'root', //'root',
                        'admin', //'',
                        array(
                            \PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                            \PDO::ATTR_PERSISTENT => false
                        )
                    );
  
    $handle = $link->prepare('SELECT  count( idTypepatente) as total ,libelle as activite FROM `patentes` INNER JOIN typepatente on patentes.idTypepatente=typepatente.id GROUP by libelle having count(idTypepatente)>10'); 
    $handle->execute(); 
    $result = $handle->fetchAll(\PDO::FETCH_OBJ);
    
    foreach($result as $row){
        array_push($dataPoints, array("x"=> $row->activite, "y"=> $row->total));
    }
  $link = null;
 
}
catch(\PDOException $ex){
    print($ex->getMessage());
}
  
?>
<!DOCTYPE HTML>
<html>
<head>  
<script>
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
  animationEnabled: true,
  exportEnabled: true,
  theme: "light1", // "light1", "light2", "dark1", "dark2"
  title:{
    text: "Moyennes des activités leaders"
  },
  data: [{
    type: "doughnut", //change type to bar, line, area, pie, etc  
   
    showInLegend: true,
    yValueFormatString: "#,##0.00\"\"",
    indexLabelPlacement: "outside",
    indexLabelFontColor: "#36454F",
    indexLabelFontSize: 18,
    indexLabel: "{activite} {y}",
    dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
  }]
});
chart.render();
 
}
</script>
</head>
<body>
<div id="chartContainer" style="height: 370px; width: 100%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</body>
</html>                         