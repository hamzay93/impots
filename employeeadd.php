<?php include('header.php'); ?>
<?php 
  
  $CountryId = 0;
  $StateId = 0;
  $CityId = 0;

  include_once('controller/connect.php');
  $dbs = new database();
  $db=$dbs->connection();


    $fname="";
    $mname="";
    $lname="";
    $bdate="";
    $mnumber="";
    $numcin="";
    $Cdate="";
    $gendern ="";
    $maritaln ="";
    $cityn ="";
    $staten ="";
    $countryn ="";
    $positionn = "";  
    $maritalid="";
    $genderId="";
    $positionidd="";
    $cityid="";
    $idTypeCb="";
  $cityn = mysqli_query($db,"select * from city  ORDER BY Name");
  $gendern = mysqli_query($db,"select * from gender  ORDER BY Name");
 
  $maritaln = mysqli_query($db,"select * from maritalstatus  ORDER BY Name");
 $positiona = mysqli_query($db,"select * from position  ORDER BY Name");

if(isset($_GET['empid']))
  {
    $contrid = $_GET['empid'];
    $edit = mysqli_query($db,"select * from tbcontribuable where id='$contrid'");
    $row = mysqli_fetch_assoc($edit);
    $fname=$row['FirstName'];
    $mname=$row['MiddleName'];
    $lname=$row['LastName'];
    $bdate=$row['Birthdate'];
    $mnumber=$row['Mobile'];
    $numcin=$row['NumCni'];
    $Cdate=$row['dateCNI'];
   $genderid = $row['Gender'];
   $maritalid = $row['MaritalStatus'];
   $cityid = $row['CityId'];
   $positionidd = $row['PositionId'];
   $idTypeCb =$row['idTypeCb'];
  
   if($idTypeCb == 2){
    ?>
<style type="text/css">
  div.particulier {
    display: none;
  }
</style>
<script>
  $(document).ready(function() {

        $('input[type=text], select').each(function() {
            $(this).prop('required', false);
        });});</script>
<?php
   }
  }
 $page = "";
  $pagelimit = 5;
  $positionid = mysqli_query($db,"select count(id) total from tbcontribuable");
  $positionn = mysqli_fetch_assoc($positionid);
  $number_of_row = ceil($positionn['total'] /10);

  if(isset($_GET['bn']) && intval($_GET['bn']) <= $number_of_row && intval($_GET['bn']) != 0)
  {
    $Skip = (intval($_GET['bn']) * $pagelimit) - $pagelimit;
    print_r($Skip);
    $sql = mysqli_query($db,"select * from tbcontribuable LIMIT $Skip,$pagelimit");
  }
  else
  {
    $sql = mysqli_query($db,"select * from tbcontribuable LIMIT $pagelimit");
  }

  for($i=0;$i<$number_of_row;$i++)
  {
    $d = $i+1;
    $page .= "<a href='employeeadd.php?bn=$d'>$d</a>&nbsp &nbsp &nbsp";
  } 
?>
<ol class="breadcrumb" style="margin: 10px 0px ! important;">
  <li class="breadcrumb-item"><a href="Home.php">Accueil</a><i class="fa fa-angle-right"></i>Contribuable<i class="fa fa-angle-right"></i>
    Nouveau contribuable</li>
</ol>
<!--grid-->
<div class="validation-system" style="margin-top: 0;">

  <div class="validation-form">
    <!---->
    <form method="POST" action="controller/ccity.php?contriedit=<?php $contrid ?>">

      <div class="vali-form-group">


        <div class="vali-form-group">
          <div class="col-md-4 control-label">
            <label class="control-label">Type contribuable*</label>
            <div class="input-group">
              <label class="radio-inline"><input type="radio" name="optradio" value="particulier" <?php
                  if(isset($_GET['empid'])){ echo ($idTypeCb==1)?'checked':'';}else {echo 'checked';}?> >Particulier</label>
              <label class="radio-inline"><input type="radio" name="optradio" value="pro" <?php
                  if(isset($_GET['empid'])){ echo ($idTypeCb==2)?'checked':'';}?>>Professionnel</label> </div>
          </div>
          <div class="clearfix"> </div>

          <div class="vali-form-group">
            <div class="col-md-4 control-label particulier">
              <label class="control-label">Prenom*</label>
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-user" aria-hidden="true"></i>
                </span>
                <input type="text" name="fname" id="required" title="First Name" value="<?php echo(isset($_GET['empid']))?$fname:""; ?>"
                  class="form-control" placeholder="Prenom" required="">
              </div>
            </div>

            <div class="col-md-4 control-label ">
              <label class="control-label">Nom 1*</label>
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-user" aria-hidden="true"></i>
                </span>
                <input type="text" name="mname" id="required" title="Middel Name" value="<?php echo(isset($_GET['empid']))?$mname:""; ?>"
                  class="form-control" placeholder="Nom 1" required="">
              </div>
            </div>

            <div class="col-md-4 control-label particulier">
              <label class="control-label">Nom 2*</label>
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-user" aria-hidden="true"></i>
                </span>
                <input type="text" name="lname" id="required" title="Last Name" value="<?php echo(isset($_GET['empid']))?$lname:""; ?>"
                  class="form-control" placeholder="Nom 2" required="">
              </div>
            </div>

            <div class="clearfix"> </div>
          </div>

          <div class="clearfix"> </div>
          <div class="vali-form-group">
            <div class="col-md-4 control-label particulier">
              <label class="control-label">Date de naissance*</label>
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-calendar" aria-hidden="true"></i>
                </span>
                <input type="text" id="Birthdate" title="Birth Date" name="bdate" placeholder="Date de naissance"
                  onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" value="<?php echo(isset($_GET['empid']))?$bdate:""; ?>"
                  class="form-control" required="">
              </div>
            </div>

            <div class="col-md-4 control-label particulier">
              <label class="control-label">Statut*</label>
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-user" aria-hidden="true"></i>
                </span>
                <select name="marital" title="Marital" style="text-transform: capitalize;">
                  <option value="">-- Selectionner statut --</option>
                  <?php while($rw = mysqli_fetch_assoc($maritaln)){ ?>
                  <option value="<?php echo $rw[" MaritalId"]; ?>"
                    <?php if(isset($_GET['empid'])){ echo ($rw["MaritalId"]==$maritalid)?'selected':'';}?>>
                    <?php echo $rw["Name"]; ?>
                  </option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-4 control-label particulier">
            <label class="control-label">Genre*</label>
            <div class="input-group">
              <span class="input-group-addon">
                <i class="fa fa-language" aria-hidden="true"></i>
              </span>
              <select name="gender" title="Genre" style="text-transform: capitalize;">
                <option value="">-- Selectionner genre --</option>
                <?php while($rw = mysqli_fetch_assoc($gendern)){ ?>
                <option value="<?php echo $rw[" GenderId"]; ?>"
                  <?php if(isset($_GET['empid']) && $genderid==$rw["GenderId"]){ echo "Selected"; }?>>
                  <?php echo $rw["Name"]; ?>
                </option>
                <?php } ?>
              </select>
            </div>


          </div>
          <div class="clearfix"> </div>

          <div class="vali-form-group">


          <div class="col-md-4 control-label particulier">
              <label class="control-label">Type de piece*</label>
              <div class="input-group">             
                  <span class="input-group-addon">
              <i class="fa fa-language" aria-hidden="true"></i>
              </span>
              <select name="city" id="required" title="Piece" style="text-transform: capitalize;" required="">
                <option value="">-- Selectionner une pièce-</option>
                <?php while($rw = mysqli_fetch_assoc($positiona)){ ?> 
                  <option value="<?php echo $rw["PositinId"]; ?>" <?php if(isset($_GET['empid']) && $cityid==$rw["PositinId"]){ echo "Selected"; }?>> <?php echo $rw["Name"]; ?> </option>
                <?php } ?>
              </select>
              </div>
                 </div>
          
          <div class="col-md-4 control-label particulier">
              <label class="control-label">Piece N°:</label>
              <div class="input-group">             
                  <span class="input-group-addon">
              <i class="fa fa-user" aria-hidden="true"></i>
              </span>
              <input type="text" id="required" name="numcin" title=" ID" value="<?php echo(isset($_GET['empid']))?$numcin:""; ?>" class="form-control" placeholder="CNI" required="">
              </div>
            </div>
            


            <div class="col-md-4 control-label particulier">
              <label class="control-label">Date de carte</label>
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-calendar" aria-hidden="true"></i>
                </span>
                <input type="text" id="Birthdate2" title="Birth Date" name="Cdate" placeholder="Date CNI" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"
                  value="<?php echo(isset($_GET['empid']))?$Cdate:""; ?>" class="form-control" required="">
              </div>
            </div>

          </div>

          <div class="col-md-4 control-label">
            <label class="control-label">Numéro mobile*</label>
            <div class="input-group">
              <span class="input-group-addon">
                <i class="fa fa-mobile" aria-hidden="true"></i>
              </span>
              <input type="text" name="mnumber" title="Mobile Number" value="<?php echo(isset($_GET['empid']))?$mnumber:""; ?>"
                class="form-control" placeholder="Mobile Number" min="10" maxlength="10" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
            </div>
          </div>

          <div class="col-md-4 control-label">
            <label class="control-label">Adresse*</label>
            <div class="input-group">
              <span class="input-group-addon">
                <i class="fa fa-language" aria-hidden="true"></i>
              </span>
              <select name="city" title="Adresse" style="text-transform: capitalize;" >
                <option value="">-- Selectionner adresse--</option>
                <?php while($rw = mysqli_fetch_assoc($cityn)){ ?>
                <option value="<?php echo $rw[" CityId"]; ?>"
                  <?php if(isset($_GET['empid']) && $cityid==$rw["CityId"]){ echo "Selected"; }?>>
                  <?php echo $rw["Name"]; ?>
                </option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div style="display:none;" class="col-md-4 control-label nif">
            <label class="control-label">NIF*</label>
            <div class="input-group">
              <span class="input-group-addon">
                <i class="fa fa-mobile" aria-hidden="true"></i>
              </span>
              <input type="text" name="nif" title="NIF" value="<?php echo(isset($_GET['empid']))?$mnumber:""; ?>"
                class="form-control" placeholder="Numero NIF" min="10" maxlength="10" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"
                >
            </div>
          </div>

          <div class="clearfix"> </div>
          <input type="hidden" id="custId" name="contriId" value="<?php echo $row['id']; ?>">

          <div class="col-md-12 form-group">
            <button type="submit" name="contriadd" class="btn btn-primary">Ajouter</button>
            <button type="reset" class="btn btn-default">Annuler</button>
          </div>
          <div class="clearfix"> </div>
    </form>
    <!---->
  </div>
</div>
<script>
  function passwordeyes() {
    var x = document.getElementById("Psw").type;
    if (x == "password")
      document.getElementById("Psw").type = "text";
    else
      document.getElementById("Psw").type = "password";
  }
</script>
<script>
  var OneStepBack;

  function nmac(val, e) {
    if (e.keyCode != 8) {
      if (val.length == 2)
        document.getElementById("mac").value = val + "-";
      if (val.length == 5)
        document.getElementById("mac").value = val + "-";
      if (val.length == 8)
        document.getElementById("mac").value = val + "-";
      if (val.length == 11)
        document.getElementById("mac").value = val + "-";
      if (val.length == 14) {
        document.getElementById("mac").value = val + "-";
      }
    }
  }

  function nmac1(val, e) {
    if (e.keyCode == 32) {
      return false;
    }

    if (e.keyCode != 8) {

      if (val.length == 2)
        document.getElementById("mac").value = val + "-";
      if (val.length == 5)
        document.getElementById("mac").value = val + "-";
      if (val.length == 8)
        document.getElementById("mac").value = val + "-";
      if (val.length == 11)
        document.getElementById("mac").value = val + "-";
      if (val.length == 14) {
        document.getElementById("mac").value = val + "-";
      }

      if (val.length == 17) {
        return false;
      }
    }
  }
</script>
<script>
  contrychange();

  function contrychange() {
    var selectedstateid = "<?php echo $StateId; ?>";
    var selectedstateid = parseInt(selectedstateid);

    var selectedcountry = $('#contryid').val();
    $.get("controller/getstatecity.php?Type=s&ci=" + selectedcountry + "&ss=" + selectedstateid, function (data, status) {
      $("#stateid").html(data);
    });
    statechange(selectedstateid);
  }

  function statechange(si) {

    var selectedstate = $('#stateid').val();
    if (si != undefined)
      selectedstate = si;

    var selectedcityid = "<?php echo $CityId; ?>";
    selectedcityid = parseInt(selectedcityid);

    $.get("controller/getstatecity.php?Type=c&si=" + selectedstate + "&sc=" + selectedcityid, function (data, status) {
      $("#cityid").html(data);
    });
  }
</script>

<script>
  $(document).ready(function () {

    $("input[name$='optradio']").click(function () {
      var test = $(this).val();
      console.log(test);
      if (test == "particulier") {
        $("div.particulier").show();
        $(".nif").hide();
        $('#Birthdate, #Birthdate2,#required').each(function () {
          $(this).prop('required', true);
        });
      } else {
        $("div.particulier").hide();
        $(".nif").show();
        $('#Birthdate, #Birthdate2,#required').each(function () {
          $(this).prop('required', false);
        });
      }
    });
  });
  var birthdate = $('#Birthdate').val();
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1;
  var yy = today.getFullYear();
  var tdate = yy + "/" + mm + "/" + dd;

  $('#Birthdate').datetimepicker({
    yearOffset: 0,
    lang: 'ch',
    timepicker: false,
    format: 'Y/m/d',
    formatDate: 'Y/m/d',
    //minDate:'-1980/01/01', // yesterday is minimum date
    maxDate: tdate // and tommorow is maximum date calendar
  });


  var birthdate = $('#Birthdate2').val();
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1;
  var yy = today.getFullYear();
  var tdate = yy + "/" + mm + "/" + dd;

  $('#Birthdate2').datetimepicker({
    yearOffset: 0,
    lang: 'ch',
    timepicker: false,
    format: 'Y/m/d',
    formatDate: 'Y/m/d',
    //minDate:'-1980/01/01', // yesterday is minimum date
    maxDate: tdate // and tommorow is maximum date calendar
  });

  $('#JoinDate').datetimepicker({
    yearOffset: 0,
    lang: 'ch',
    timepicker: false,
    format: 'Y/m/d',
    formatDate: 'Y/m/d',
    //minDate:'-1980/01/01', // yesterday is minimum date
    //maxDate: tdate // and tommorow is maximum date calendar
  });

  $('#LeaveDate').datetimepicker({
    yearOffset: 0,
    lang: 'ch',
    timepicker: false,
    format: 'Y/m/d',
    formatDate: 'Y/m/d',
    //minDate:'-1980/01/01', // yesterday is minimum date
    //maxDate: tdate // and tommorow is maximum date calendar
  });
</script>
<?php include('footer.php'); ?>