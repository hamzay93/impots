
<?php include('header.php'); ?>
<?php 
  include_once('controller/connect.php');
  
  $dbs = new database();
  $db=$dbs->connection();
  $name=""; 
  $id="";
  $idStock="";
 

  $page="";
	$RecordeLimit = 5;
	$stockAll = mysqli_query($db,"select count(idStock) as total from stockVignette ");
	$CName = mysqli_fetch_array($stockAll);

	$number_of_row =ceil($CName['total']/5);
	if(isset($_GET['bn']) && intval($_GET['bn']) <= $number_of_row && intval($_GET['bn'] != 0 ))
	{
	$Skip = (intval($_GET["bn"]) * $RecordeLimit) - $RecordeLimit;
	$sql = mysqli_query($db,"select * from stockVignette LIMIT $Skip,$RecordeLimit");
	}
	else
	{
	$sql = mysqli_query($db,"select * from stockVignette LIMIT $RecordeLimit");
	}

	for($i=0;$i<$number_of_row;$i++)
	{
	$d = $i+1;
	$page .= "<a href='city.php?bn=$d'>$d</a>&nbsp &nbsp &nbsp";
	}
?>
<link rel="stylesheet" type="text/css" href="css/table-style.css" />
<link rel="stylesheet" type="text/css" href="css/basictable.css" />
<script type="text/javascript" src="js/jquery.basictable.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#table').basictable();

      $('#table-breakpoint').basictable({
        breakpoint: 768
      });

      $('#table-swap-axis').basictable({
        swapAxis: true
      });

      $('#table-force-off').basictable({
        forceResponsive: false
      });

      $('#table-no-resize').basictable({
        noResize: true
      });

      $('#table-two-axis').basictable();

      $('#table-max-height').basictable({
        tableWrapper: true
      });
    });
</script>
<ol class="breadcrumb" style="margin: 10px 0px ! important;">
    <li class="breadcrumb-item"><a href="Home.php">Accueil</a><i class="fa fa-angle-right"></i>Parametrage<i class="fa fa-angle-right"></i>Stock</li>
</ol>

<div class="validation-system" style="margin-top: 0;">
    
    <div class="validation-form" style="overflow: auto; margin-right:20px; height: 450px; width: 49%; float: left;">
  <!---->
        <form method="POST" action="controller/ccity.php?cityedit=<?php $idCity ?>">
        <div class="vali-form-group" >
        <h2>Modification</h2>
                     <div class="col-md-3 control-label">
              <label class="control-label">Serie A</label>
                <div class="input-group">             
                    <span class="input-group-addon">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                  </span>
                <input type="text" name="serieA" placeholder="Serie A" required="" class="form-control" style="width: 250px; height: 35px;">
                </div>
            </div>
            <div class="clearfix"> </div>
            <div class="col-md-3 control-label">
              <label class="control-label">Serie B</label>
                <div class="input-group">             
                    <span class="input-group-addon">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                  </span>
                <input type="text" name="serieB" placeholder="Serie B" required="" class="form-control" style="width: 250px; height: 35px;">
                </div>
            </div>
           <div class="clearfix"> </div>
           <div class="col-md-3 control-label">
              <label class="control-label">Serie C</label>
                <div class="input-group">             
                    <span class="input-group-addon">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                  </span>
                <input type="text" name="serieC" placeholder="Serie C" required="" class="form-control" style="width: 250px; height: 35px;">
                </div>
            </div>
            <div class="clearfix"> </div>
       

            <div class="col-md-3 control-label">
              <label class="control-label">Serie D</label>
                <div class="input-group">             
                    <span class="input-group-addon">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                  </span>
                <input type="text" name="serieD" placeholder="Serie D" required="" class="form-control" style="width: 250px; height: 35px;">
                </div>
            </div>
              <div class="clearfix"> </div>
              <input type="hidden" id="custId" name="stockId" value="<?php echo $row['idStock']; ?>">

        </div>
            <div class="col-md-12 form-group">
              <button type="submit" name="stock" class="btn btn-primary">Modifier</button>              
            </div>
          <div class="clearfix"> </div>
        </form>
  <!---->
 </div>
 <div class="validation-form" style="width: 49%; overflow: auto;">
    <div style="height: 396px;">
          <div class="w3l-table-info" >
            <h2>Stock</h2>
            <br>
              <table id="table">
            <thead>
              <tr>
              <th style="width: 5000px;">Serie</th>
              <th style="width: 5000px;">Stock final</th>
              </tr>
            </thead>
            <tbody>
            <?php $i=1; while($row = mysqli_fetch_assoc($sql)) { ?> 
            <tr>
              <td><?php echo ucfirst($row['Serie']); ?></td>
              <td><?php echo ucfirst($row['stockFinal']); ?></td>
             </tr>  
              <?php } ?>
            </tbody>
            </table>
            <div><?php echo $page; ?></div>
          </div>
    </div>
 </div>
</div>
<?php include('footer.php'); ?>