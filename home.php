<?php include('header.php'); ?>
<?php
  include_once('controller/connect.php');
  
  $dbs = new database();
  $db=$dbs->connection();
  $TotalEmp =mysqli_query($db,"select count(id) as emp from tbcontribuable");
  $Totalpat =mysqli_query($db,"select count(patenteId) as emp from patentes");
   $Totalvig =mysqli_query($db,"select count(idVignette) as emp from vignette");
  $TotalEmploId = mysqli_fetch_assoc($TotalEmp);
   $TotalpatenId = mysqli_fetch_assoc($Totalpat);
   $TotalvigId = mysqli_fetch_assoc($Totalvig);

?>
<ol class="breadcrumb" style="margin: 10px 0px ! important;">
    <li class="breadcrumb-item" title="Home"><a href="index.php">Accueil</a></li>
</ol>
<!--four-grids here-->
		<div class="four-grids" style="margin-bottom: 30px; margin-top: 10px; background: white; ">
					<div class="col-md-3 four-grid">
						<div class="four-agileits">
						<a href="employeeview.php">
							<div class="icon">
								<i class="glyphicon glyphicon-user" aria-hidden="true"></i>
							</div>
							<div class="four-text">
								<h3>Contribuables</h3>
								<h4><?php echo(isset($TotalEmploId['emp']))?$TotalEmploId['emp']:"";?></h4>
							</div>
						</a>
							
						</div>
					</div>
					<div class="col-md-3 four-grid">
						<div class="four-agileinfo">
						<a href="patenteview.php">
							<div class="icon">
								<i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i>
							</div>
							<div class="four-text">
								<h3>Patentes</h3>
								<h4><?php echo(isset($TotalpatenId['emp']))?$TotalpatenId['emp']:"";?></h4>
							</div>
						</a>
						</div>
					</div>

						<div class="col-md-3 four-grid">
						<div class="four-agileinfos">
						<a href="listevignette.php">
							<div class="icon">
								<i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i>
							</div>
							<div class="four-text">
								<h3>Vignettes</h3>
								<h4><?php echo(isset($TotalvigId['emp']))?$TotalvigId['emp']:"";?></h4>
							</div>
						</a>
						</div>
					</div>
					<!-- <div class="col-md-3 four-grid">
						<div class="four-w3ls">
							<div class="icon">
								<i class="glyphicon glyphicon-folder-open" aria-hidden="true"></i>
							</div>
							<div class="four-text">
								<h3>Projects</h3>
								<h4>12,430</h4>
								
							</div>
							
						</div>
					</div> -->
					<!-- <div class="col-md-3 four-grid">
						<div class="four-wthree">
							<div class="icon">
								<i class="glyphicon glyphicon-briefcase" aria-hidden="true"></i>
							</div>
							<div class="four-text">
								<h3>Old Projects</h3>
								<h4>14,430</h4>
								
							</div>
							
						</div>
					</div> -->
					<div class="clearfix"></div>
				</div>
<!--//four-grids here-->


  
<?php include('footer.php'); ?>

