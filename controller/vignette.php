<?php
	include_once('connect.php');
	session_start();

	$dbs = new database();
	$db=$dbs->connection();
	if(isset($_POST['submit']))
	{
		$gender=$_POST['gender'];
        $fname=$_POST['fname'];
        $mname=$_POST['mname'];
        $lname=$_POST['lname'];
        $bdate=$_POST['bdate'];
        $mnumber=$_POST['mnumber'];
        $numcin=$_POST['numcin'];
        $Cdate=$_POST['Cdate'];
		$position=$_POST['position'];
		$type=$_POST['optradio'];
		$nif=$_POST['nif'];
		$typeId='';
		if ($type == 'particulier'){
			$typeId = 1;
		}else {
			$typeId= 2;
		}
    	$city=$_POST['city'];
		//Existence du contribuable
		$idCb="";
		$sqlExist="";
		if($type == 'particulier'){
			$idParticulier =$_POST['numcin'];
			$sqlExist = mysqli_query($db,"select id from tbcontribuable where NumCni='$idParticulier'");
		} else{
			$idPro =$_POST['mnumber'];
			$sqlExist = mysqli_query($db,"select id from tbcontribuable where nif='$nif'");
		}
        if(mysqli_num_rows($sqlExist))
        {
			$rowUser = mysqli_fetch_assoc($sqlExist);
			$idCb = $rowUser['id'];
        }
        else
        {
        	if($type == 'particulier'){
            mysqli_query($db,"insert into tbcontribuable(id,firstName,MiddleName,lastName,Birthdate,gender,cityId,mobile,positionId,NumCni,dateCNI,idTypeCb) values(null,'$fname','$mname','$lname','$bdate','$gender','$city','$mnumber','$position','$numcin','$Cdate','$typeId')")or die (mysqli_error($db));
			$idCb = mysqli_insert_id($db);

			}else {
        		mysqli_query($db,"insert into tbcontribuable(id,firstName,MiddleName,lastName,CityId,Mobile,idTypeCb,nif) values(null,null,'$mname','$lname','$city','$mnumber','$typeId','$nif')")or die (mysqli_error($db));
				$idCb = mysqli_insert_id($db);
			}
        }

		$idUser = $_SESSION['User']['id'];
		$name = $_SESSION['User']['nom'];
		$userByCentre = mysqli_query($db,"select idCentre from user where id='$idUser'");
		$rw = mysqli_fetch_assoc($userByCentre);
		$idCentre= $rw['idCentre'];
		$date = date("Y-m-d");
		$imm = $_POST['imm'];
		$statut =$_POST['statut'];
		$puissance = $_POST['puissance'];
		$dateFirst = $_POST['dateFirst'];
		$dateLast = $_POST['dateLast'];
		$montant = $_POST['montant'];
		$modepaye = $_POST['modepaye'];
		$sql = mysqli_query($db,"select serieId,libelle from serie where montant='$montant'");
		$row = mysqli_fetch_assoc($sql);
		$idSerie = $row['serieId'];
		$idPen=$_POST['pen'];
		$idPen = !empty($idPen) ? "'$idPen'" : "NULL";
		$mtPen=$_POST['mtPen'];
		$mtPen = !empty($mtPen) ? "'$mtPen'" : "NULL";
		if($modepaye==1){
		mysqli_query($db,"insert into vignette(	idVignette,	carteGrise,puissance,dateDebut,	
		dateFin,idSerie,idContribuable,	idStatus,addedby,addedate,idcenter,immatriculation,modepaye, `idPenalite`, `montantPenalite`) values(null,null,'$puissance','$dateFirst','$dateLast','$idSerie','$idCb','$statut',
		'$name','$date','$idCentre','$imm','$modepaye',$idPen,$mtPen)")or die(mysqli_error($db));}

		else
		{
			$mont = $_POST['mont'];
			$numb = $_POST['numb'];
            $banque= $_POST['banque'];
			mysqli_query($db,"insert into cheque(id,numerocheque,montant,BANQUE_id)values(null,'$numb','$mont','$banque')") or die(mysqli_error($db));
			$insertId = mysqli_insert_id($db);
			mysqli_query($db,"insert into vignette(	idVignette,	carteGrise,puissance,dateDebut,	
		dateFin,idSerie,idContribuable,	idStatus,addedby,addedate,idcenter,immatriculation,modepaye, `idPenalite`, `montantPenalite`,`idcheque`) values(null,null,'$puissance','$dateFirst','$dateLast','$idSerie','$idCb',
		'$statut','$name','$date','$idCentre','$imm','$modepaye',$idPen,$mtPen,'$insertId')")or die(mysqli_error($db));


		}
		mysqli_query($db,"update stockvignette set stockfinal=stockfinal - 1 where serie='$row['libelle']'");
		echo "<script>window.location='../vignetteadd.php';</script>";


	}
	if(isset($_POST['modify']))
	{
		$date = date("Y-m-d");
		$name = $_SESSION['User']['nom'];
		$statut =$_POST['statut'];
		$carteGrise = $_POST['carteGrise'];
		$puissance = $_POST['puissance'];
		$dateFirst = $_POST['dateFirst'];
		$dateLast = $_POST['dateLast'];
		$imm = $_POST['immatriculation'];
		$contribuableId = $_POST['contribuableId'];
		$montant = $_POST['montant'];
		$modepaye = $_POST['modepaye'];
		$sql = mysqli_query($db,"select serieId from serie where montant='$montant'");
		$row = mysqli_fetch_assoc($sql);
		$idSerie = $row['serieId'];
		$id = $_POST['IdVignette'];
		mysqli_query($db,"update vignette set carteGrise='$carteGrise',puissance='$puissance',dateDebut='$dateFirst',
		dateFin='$dateLast',idSerie='$idSerie',idStatus='$statut', modifyby='$name',
		modifydate='$date',immatriculation='$imm',modepaye='$modepaye' where idVignette = '$id'") or die(mysqli_error($db));
		echo "<script>window.location='../listeVignette.php';</script>";


	}

?>